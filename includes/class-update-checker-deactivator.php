<?php

/**
 * Fired during plugin deactivation
 *
 * @link       cloud3dots.com
 * @since      0.1.0
 *
 * @package    Update_Checker
 * @subpackage Update_Checker/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.1.0
 * @package    Update_Checker
 * @subpackage Update_Checker/includes
 * @author     cloud3dots <cloud3dots@gmail.com>
 */
class Update_Checker_Deactivator
{
    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    0.1.0
     */
    public static function deactivate()
    {
    }
}
