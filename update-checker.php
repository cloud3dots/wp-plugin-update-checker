<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              cloud3dots.com
 * @since             0.1.0
 * @package           Update_Checker
 *
 * @wordpress-plugin
 * Plugin Name:       Update Checker
 * Plugin URI:        https://gitlab.com/cloud3dots/wp-plugin-update-checker
 * Description:       Updates custom Plugins and Themes from a Git repository. It works with GitHub, GitLab and BitBucket.
 * Version:           0.1.1
 * Author:            cloud3dots
 * Author URI:        cloud3dots.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       update-checker
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (! defined('WPINC')) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 0.1.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('UPDATE_CHECKER_VERSION', '0.1.0');

/**
 * Load Plugin Update Checker.
 * Start
 */

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-update-checker-activator.php
 */
function activate_update_checker()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-update-checker-activator.php';
    Update_Checker_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-update-checker-deactivator.php
 */
function deactivate_update_checker()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-update-checker-deactivator.php';
    Update_Checker_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_update_checker');
register_deactivation_hook(__FILE__, 'deactivate_update_checker');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-update-checker.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.1.0
 */
function run_update_checker()
{
    $plugin = new Update_Checker();
    $plugin->run();
}
run_update_checker();
