<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       cloud3dots.com
 * @since      0.1.0
 *
 * @package    Update_Checker
 * @subpackage Update_Checker/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
